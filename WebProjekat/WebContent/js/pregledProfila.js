var selectedUser = "";

$(document).on('click', '#viewProfile_btn', function(e){
	e.preventDefault();
	window.location = '../static/profil.html?username='+currentUser.username+'';
});

function drawUserInfo(data){
	var user = data;
	var date = new Date(user.birthDate);
	$(".main").empty();
	var element = '<div> <h1 <p>'+user.username+'</p></h1>';
	element += '<img id="profilePic" src = "'+user.profileImageUrl+'" width= "411px" height= "321">';
	element += '<h2 <p> ime i prezime : '+user.name +' '+user.surname+'</p></h2>';
	element += '<h2 <p> datum rodjenja : '+date.toISOString().substring(0,10)+ '</p></h2></div>';
	return element;
}

function drawCurrentUser(data){
	$(".main").append(drawUserInfo(data));
	var user = data;
	var element = '<div>';
	element +='<button class="sameRowButton" type="button" value= "toma" id="addPost_btn">Dodaj objavu</button>';
	element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="photos_btn">Slike</button> </div>';
	$(".main").append(element);
	drawPosts(user.posts)
	}
	
	
$(document).on('click', '#addPost_btn', function(e){
	window.location='../static/kreiranjeObjave.html';
	
});

$(document).on('click', '#submitPost_btn', function(e){
	e.preventDefault();
	var file = $("#Image").val();
	var array = file.split("\\");
	var fileName = array[2];
	var text = $('textarea').val();
	if(fileName == null){
		fileName = null;
		if(text == ""){
			alert("Morate uneti tekst ili sliku!");
			return;
		}}

	$.ajax({
		type : 'POST',
		url : "../rest/post/postPost",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"picture":fileName,"text":text}),
		success : function(){alert("Uspesno postavljanje posta!");},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
});


$(document).on('click', '#photos_btn',function(e){
	
		var username = $(this).val();
		if(currentUser.isAdmin){
			window.location='../static/galerijaAdmin.html?username='+username+'';
		}
		else{
			window.location='../static/galerija.html?username='+username+'';
		}
		

});


$(document).on('click', '#chat_btn',function(e){
		var username = $(this).val();
		if(currentUser.isAdmin){
			window.location='../static/chatAdmin.html?username='+username+'';
		}
		else
		{
			window.location='../static/chat.html?username='+username+'';
		}
		
});


function createGallery(data){
	$(".main").empty();
	$(".main").append('<h1>Galerija<h1>')
	var posts = data == null ? [] : (data instanceof Array? data:[data]);
	
	$.each(posts, function(index, post){
		if(post.picture!=null){
			createPicture(post);
		}
	});
	
}
function createPicture(post){
	var element = '<div class="gallery">\
  <a target="_blank" href="'+post.id+'" id="postLink_btn">\
    <img src="'+post.picture+'" width="600" height="400"> </a></div>'
  $(".main").append(element);
}

$(document).on('click', '#postLink_btn', function(e){
	e.preventDefault();
	var id = $(this).attr('href');
	if(currentUser.isAdmin){
		window.location.href='../static/postAdmin.html?id='+id+'';
	}
	else{
	window.location.href='../static/post.html?id='+id+'';
	}
	});


function drawOther(username){
	$.ajax({
		type : 'POST',
		url : "../rest/request/getUserToPresent",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username" : username}),
		success : drawOtherProfile,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
}

function drawOtherProfile(data){

	$(".main").empty();
	$(".main").append(drawUserInfo(data.user));
	var user = data.user;
	var element = '';
	if (currentUser.isAdmin){
		if(user.isDeleted){
				element +='<button class="sameRowButton" type="button" value="'+user.username+'"id="unblockUser_btn"> Odblokiraj Korisnika</button>';

		}
		else{
			element +='<button class="sameRowButton negativeButton" type="button" value="'+user.username+'"id="blockUser_btn">Blokiraj Korisnika</button>';
		}
		element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="chat_btn">Poruke</button> ';
		element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="photos_btn">Slike</button>';
		$(".main").append(element);
			drawPosts(user.posts);
			return;
	}
	else if (!data.login){
		if(user.isPrivate){
			element+= '<h1>Ovaj profil je privatan<h1>'
		}
		else{
			element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="photos_btn">Slike</button>';
			$(".main").append(element);
			drawPosts(user.posts);
			return;
		}
	}
	else{
		if(!data.friend){
			if(data.sent){
				element +='<button class="sameRowButton negativeButton"  type="button" value="'+user.username+'"id="cancelRequest_btn">Otkazi Zahtev za Prijateljstvo</button>';
			}
			else if (data.recived){
				element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="accept_btn">Prihvati Zahtev</button>';
				element += '<button class="sameRowButton negativeButton" type="button" value="'+user.username+'"id="decline_btn">Odbi Zahtev</button>';
			}
			else{
			element +='<button class="sameRowButton" type="button" value="'+user.username+'"id="friendRequest_btn">Dodaj za Prijatelja</button>';
			}
			element +='<button class="sameRowButton" type="button" value="'+user.username+'"id="mutualFriends_btn">Zajednicki Prijatelji</button>';	
	
			if(user.isPrivate){
			element+= '<h1>Ovaj profil je privatan<h1>'
			}
			
		}
		else{
			element +='<button class="sameRowButton negativeButton" type="button" value="'+user.username+'"id="removeFriend_btn">Prekini Prijateljstvo</button>';
			element += '<button class="sameRowButton" type="button" value="'+user.username+'"id="chat_btn">Poruke</button> ';
			element +='<button class="sameRowButton" type="button" value="'+user.username+'"id="mutualFriends_btn">Zajednicki Prijatelji</button>';	
		}
 		$(".main").append(element);
		if (!user.isPrivate || data.friend){
			element = '<button class="sameRowButton" type="button" value="'+user.username+'"id="photos_btn">Slike</button>';
			$(".main").append(element);
			drawPosts(user.posts);
		}
			
	}
	
}
	
$(document).on('click', '#friendRequest_btn', function(e){
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/request/sendRequest",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				drawOther(username);
				alert("Poslali ste zahtev korisniku :  " + username );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});


$(document).on('click', "#removeFriend_btn", function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/user/removeFriend",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				drawOther(username);
				alert("Niste vise prijatelj sa  " + username );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});
	
$(document).on('click','#cancelRequest_btn', function(e){
 	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/request/cancelRequest",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				drawOther(username);
				alert("ponisten zahtev za prijateljstvo upucen korisniku :" + username );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});
$(document).on('click', '#link_btn', function(e){
	e.preventDefault();
	var u = $(this).attr('href');
	
	if(currentUser == null){
		window.location='../static/profilNeregistrovan.html?username='+u+'';
	}
	else if(currentUser.isAdmin){
		window.location='../static/profilAdmin.html?username='+u+'';
	}
	else{
		window.location='../static/profil.html?username='+u+'';
	}
});

$(document).on('click', '#blockUser_btn', function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/user/blockUser",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				drawOther(username);
				alert("Korisnik " + username + " je uspesno blokiran" );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});
	

$(document).on('click', '#unblockUser_btn', function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/user/unblockUser",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				drawOther(username);
				alert("Korisnik " + username + " je uspesno odblokiran" );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});

	
	