var searchUsers = [];



$(document).on('submit', '#search_form', function(e){
	e.preventDefault();
	var name = $(this).find("input[name=name]").val();
	var surname = $(this).find("input[name=surname]").val();
	var dateFrom = $(this).find("input[name = birthDateFrom]").val();
	var dateTo = $(this).find("input[name = birthDateTo]").val();
	
	if(name == "" && surname =="" && dateFrom =="" && dateTo ==""){
		alert("Morate uneti neki od parametara za pretrazivanje");
		return;
	}
	
	if((dateFrom == "" && dateTo != "") || (dateFrom != "" && dateTo =="")){
		alert("Morate uneti i datum od i datum do");
		return;
	}
	if(dateFrom == "" && dateTo == ""){
		dateFrom = null;
		dateTo = null;
	}
	
	$.ajax({
		type : 'POST',
		url : "../rest/user/search",
		contentType: 'application/json',
		dataType : 'json',
		data: formToJSON_search(name, surname, dateFrom, dateTo),
		success : searchResult,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	

	
});
$(document).on('submit', '#sortForm', function(e){
	e.preventDefault();
	
	var sortParameter = $('#sortParameter').find(":selected").val();
	alert(sortParameter);
	
	if(searchUsers!=[]){
		if(sortParameter =="name"){
			searchUsers.sort((a,b) => (a.name > b.name)? 1 :-1);
		}
		else if(sortParameter =="surname"){
			searchUsers.sort((a,b) => (a.surname > b.surname)? 1 :-1);
		}
		else if(sortParameter =="date"){
			searchUsers.sort((a,b) => (a.birthDate > b.birthDate)? 1 :-1);
		}
		$(".main").empty();
		$.each(searchUsers,function(index, user){
			drawUser(user);
	});
	}
	
});



function searchResult(data){
	$(".main").empty();
	searchUsers = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(searchUsers,function(index, user){
		drawUser(user);
	});
	
}



function formToJSON_search(name, surname, dateFrom, dateTo){
	return JSON.stringify({
		"name":name,
		"surname":surname,
		"dateFrom":dateFrom,
		"dateTo":dateTo
	});
}