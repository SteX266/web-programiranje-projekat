var currentUser = {};


$(document).ready(function(){
	loadCurrentUser();
	
	if (window.location.href=="http://localhost:8080/WebProjekat/static/korisnik.html"){	
	$.ajax({
		type : 'GET',
		url : '../rest/post/getFriendsPosts',
		dataType: 'json',
		success : drawPosts,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("bas odavde dobijam AJAX ERROR: " + errorThrown);
		}
	});
	}
	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/izmenaProfila.html")){
		$.ajax({
			type : 'GET',
			url : '../rest/user/getCurrentUser',
			dataType : 'json',
			success : showUserInfo,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX ERROR: " + errorThrown);
			}
		});
	}
	else if(window.location.href=="http://localhost:8080/WebProjekat/static/admin.html"){
		$.ajax({
		type : 'GET',
			url : '../rest/user/getAllUsers',
			dataType : 'json',
			success : drawAllUsers,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX ERROR: " + errorThrown);
			}
	});
	}
	else if (window.location.href.startsWith("http://localhost:8080/WebProjekat/static/profil")){
		var url_string = window.location.href;
		var url = new URL(url_string);
		var username = url.searchParams.get("username");
		
		$.ajax({
		type : 'GET',
		url : '../rest/user/getCurrentUser',
		success : function(user){
			if(user == null){
				drawOther(username);
			}
			else if(user.username == username){
				drawCurrentUser(user);
			}
			else{
				drawOther(username);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
		
	}
	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/galerija")){
		loadCurrentUser();
		var url_string = window.location.href;
		var url = new URL(url_string);
		var username = url.searchParams.get("username");
		
		
		$.ajax({
		type : 'POST',
		url : '../rest/post/getUserPosts',
		contentType:'application/json',
		dataType: 'json',
		data:JSON.stringify({"username":username}),
		success : createGallery,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("bas odavde dobijam AJAX ERROR: " + errorThrown);
		}
	});	
	}
	else if(window.location=="http://localhost:8080/WebProjekat/static/listaPrijatelja.html"){
		$.ajax({
		type : 'GET',
		url : '../rest/user/getFriendList',
		success : drawFriends,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	
	}
	else if(window.location=="http://localhost:8080/WebProjekat/static/listaZahteva.html"){
		requests();
}

	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/post")) {
		
		var url_string = window.location.href;
		var url = new URL(url_string);
		var id = url.searchParams.get("id");
		$.ajax({
		type : 'POST',
		url : "../rest/post/getPostById",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"id":id}),
		success : detailedPostShow,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	}
	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/kreiranjeObjave.html")){
		
	$(".main").empty();
	$(".main").append('<h1 <p>Postavljanje objave</p></h1><p>Odaberite sliku:</p>');
	$(".main").append('<input type="file" name = "image" id = "Image" accept="image/*">');
	drawCommentBox(true);
	}
	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/listaChatova")){
		
	$(".main").empty();
	$(".main").append('<h1 <p>Lista Dopisivanja</p></h1>');
	drawUserChats();
	}

	else if(window.location.href.startsWith("http://localhost:8080/WebProjekat/static/chat")){
		
		var url_string = window.location.href;
		var url = new URL(url_string);
		var username = url.searchParams.get("username");
		drawChatUser(username);
		
		
	}

});
	

function drawAllUsers(data){
	$(".main").append("<h1>Lista svih korisnika</h1>");
	var allUsers = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(allUsers, function(index, user){
		if(!user.isDeleted){
		drawUser(user);
		}
	
	});
}

function loadCurrentUser(){
	$.ajax({
			type : 'GET',
			url : '../rest/user/getCurrentUser',
			dataType : 'json',
			success : function(data){
				currentUser = data;
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX ERROR: " + errorThrown);
			}
		});
	
}

function drawPosts(data){
	
	$(".main").append('<h1>Postovi<h1>');
	
	var posts = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(posts, function(index, post){
		if(post.picture == null){
			drawPost(post)
		}
		else{
			drawPicture(post);
		}
	});
	
}
function drawPost(post){
	if(post.isDeleted){
		return;
	}
	var element =  '<div class="post mock-outer">';
	element+= '<div class="mock-inner">';
	element += '<div class="fb-group-picrow">';
	element += '<img src="'+post.userPicture+'">';
	element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
	element += '<a href="'+post.username+'" id="link_btn"><h5 class="fbh5">'+post.username+'</h5></a></div></div></div>';
	element += '<div class="usertext"><p>'+post.text+'</p></div>';
	element += '<button class="button-pretty" type="button" value="'+post.id+'"id="comments_btn">Komentari</button>';
	if(currentUser.isAdmin == true || currentUser.username == post.username){
	element += '<button class="button-pretty" type="button" value="'+post.id+'" id="deletePost_btn">Obrisi</button>';
	}	
	element+= '</div>';
	$(".main").append(element);
	
	
}
function drawPicture(post){
		if(post.isDeleted){
			return;
		}
		var element =  '<div class="mock-outer">';
		element+= '<div class="mock-inner">';
		element += '<div class="fb-group-picrow">';
		element += '<img src="'+post.userPicture+'">';
		element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
		element += '<a href="'+post.username+'" id="link_btn"><h5 class="fbh5">'+post.username+'</h5></a></div></div></div>';
		element += '<div class="usertext"><p>'+post.text+'</p></div> ';
		element +='<div class="mock-img-all">';
		element += '<div class="mock-img"><img class="slika" src="'+post.picture+'"></div></div>';
		element += '<button class="button-pretty" type="button" value="'+post.id+'"id="comments_btn">Komentari</button>';
		if(currentUser.isAdmin == true || currentUser.username == post.username){
			element += '<button class="button-pretty" type="button" value="'+post.id+'" id="deletePost_btn">Obrisi</button>';
		}	
		element+= '</div>';		
		$(".main").append(element);
		
		
}




$(document).on('click', '#comments_btn', function(e){
	e.preventDefault();
	var id = $(this).val();
	if(currentUser.isAdmin){
		window.location.href='../static/postAdmin.html?id='+id+'';
	}
	else{
		window.location.href='../static/post.html?id='+id+'';
	}
	
	

	});
	
function detailedPostShow(post){
	$(".main").empty();
	
	if(post.picture==null){
		drawPost(post);
	}
	else{
		drawPicture(post);
	}
	$('#comments_btn').toggle();
	drawPostComments(post);
	
	
	
	drawCommentBox(false);
}


function drawCommentBox(isPost){
	var element='\
	<section id="app">\
    <div class="container">\
      <div class="row">\
        <div class="col-6">\
          <div class="comment">\
        <p v-for="items in item" v-text="items"></p>\
          </div><!--End Comment-->\
          </div><!--End col -->\
          </div><!-- End row -->\
      <div class="row">\
        <div class="col-6">\
      <textarea type="text" class="input" placeholder="';
      if(isPost){
		element += 'Napisite post';
	}
	else{
		element+='Napisite komentar';
	}
      element+='" v-model="newItem" @keyup.enter="addItem() id="comment_form"></textarea>\
          <button class="primaryContained float-right" type="submit" id="';
      if(isPost){
		element += 'submitPost_btn';
	}
	else{
		element += 'submitComment_btn';
	}
      element +='">Objavi</button>\
        </div><!-- End col -->\
      </div><!--End Row -->\
    </div><!--End Container -->\
  </section><!-- end App -->\
  ';
  $(".main").append(element);
}

$(document).on('click', '#submitComment_btn', function(e){
	e.preventDefault();
	var text = $('textarea').val();
	
	var postId = $("#comments_btn").val();
	
	
	$.ajax({
		type : 'POST',
		url : "../rest/comment/postComment",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"text":text,"postId":postId}),
		success : drawComment,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	
	
	});

function drawPostComments(post){
	
	$.each(post.comments, function(i){
		drawComment(post.comments[i]);
	});
	
}

function drawComment(comment){
	if(comment.isDeleted){
		return;
	}
	
	 $.ajax({
		type : 'POST',
		url : "../rest/user/getUserByUsername",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":comment.user}),
		success : user = function(data){
			
			var element =  '<div class="userShowcase mock-outer">';
			element+= '<div class="mock-inner">';
			element += '<div class="fb-group-picrow">';
			element += '<img src="'+data.profileImageUrl+'">';
			element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
			element += '<a href="'+comment.user+'" id="link_btn"><h5 class="fbh5">'+comment.user+'</h5></a></div></div></div>';
			element += '<div class="inline usertext"><p>'+comment.text +'</p></div>'
			if(currentUser.isAdmin == true || currentUser.username == comment.user){
				element += '<button class="button-pretty" type="button" value="'+comment.id+'" id="deleteComment_btn">Obrisi</button>';
			}	
			
			element += '</div>';
			$(".main").append(element);
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
}
$(document).on('click', '#deletePost_btn', function(e){
	var id = $(this).val();
	
	$.ajax({
		type : 'POST',
		url : "../rest/post/deletePost",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"id":id}),
		success : function(){
			alert("Uspesno ste obrisali post");
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	
});

$(document).on('click', '#deleteComment_btn', function(e){
	
	var id = $(this).val();
	
	$.ajax({
		type : 'POST',
		url : "../rest/comment/deleteComment",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"id":id}),
		success : function(){
			alert("Uspesno ste obrisali komentar");
			
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	
});


function getUserByUsername(username){
	var user;

	return user;
}

$(document).on('click', '#logout_btn', function(e){
	e.preventDefault();
	currentUser = {};
	$.ajax({
		type : 'GET',
		url : '../rest/user/logout',
		success : function(){
			window.location = "../prijava.html"
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	
});


function showUserInfo(user){

	$('input[name = username]').val(user.username);
	$('input[name = username]').prop("readonly",true);
	
	$('input[name = password]').val(user.password);
	$('input[name = email]').val(user.email);
	$('input[name = name]').val(user.name);
	$('input[name = surname]').val(user.surname);
	
	$("#profilePic").attr("src",user.profileImageUrl);
	
	var date = new Date(user.birthDate);
	
	
	$('input[name = birthDate]').val(date.toISOString().substring(0,10));
	
	if(user.gender == "Zensko"){
		$('#Gender').val("zensko");
	}
	
	if (user.isPrivate == true){
		$('input[name = isPrivate]').prop('checked', true);
	}
	
}

$(document).on('submit', '#formaIzmena', function(e){
	e.preventDefault();
	
	var username = $(this).find("input[name = username]").val();
	var password = $(this).find("input[name = password]").val();
	var name = $(this).find("input[name = name]").val();
	var surname = $(this).find("input[name = surname]").val();
	var email = $(this).find("input[name = email]").val();
	var birthDate = $(this).find("input[name = birthDate]").val();
	var gender = $('#Gender').find(":selected").text();
	var isPrivate = $(this).find("input[name=isPrivate]").is(":checked")?"true":"false";
	
	var file = $("#Image").val();
	
	var array = file.split("\\");
	
	var fileName = array[2];
	
	
	if (password == "" || name == "" || surname == "" || email == "" || birthDate == "" || gender ==""){
		alert("Morate popuniti sva polja!");
		return false;
		}
	$.ajax({
		type : 'POST',
		url : "../rest/user/changeUserInfo",
		contentType: 'application/json',
		dataType : 'json',
		data: formToJSON_reg(username, password,  name,  surname,  email,  birthDate, gender, isPrivate),
		success : function(){;
			if(fileName != undefined){
				
				
				uploadImage(fileName);
			}
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	
	
	
	function uploadImage(fileName){
	$.ajax({
		url : '../rest/user/changeImage',
        type : "POST",
        contentType : "application/json",
        data : JSON.stringify({"profileImageUrl":fileName}),
        processData : false,
        success: function(){
        	alert("Uspesna izmena slike!");
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	}); 
}
	
	
	});
function formToJSON_reg(username, password,  name,  surname, email, birthDate, gender,isPrivate ) {
	return JSON.stringify({
		"username" : username,
		"password" : password,
		"name" : name,
		"surname" : surname,
		"email" : email,
		"birthDate" : birthDate,
		"gender" : gender,
		"isPrivate":isPrivate
	});
}


