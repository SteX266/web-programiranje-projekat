var rootURL_log = "../WebProjekat/rest/user/login";
var rootURL_reg = "../rest/user/register"


$(document).ready(function(){
	currentUser={};
	
	
	
});
$(document).on('submit', '#formaPrijava', function(e){
	var username = $(this).find("input[name = korisnickoIme]").val();
	var password = $(this).find("input[name = lozinka]").val();
	if (username == "" || password == ""){
		alert("Morate popuniti sva polja prijave!")
		return false;
	}
	e.preventDefault();
	console.log("prijava");
	$.ajax({
		type : 'POST',
		url : rootURL_log,
		contentType: 'application/json',
		dataType : 'json',
		data: formToJSON_log(username, password),
		success : rezultatPrijave,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	
})


$(document).on('submit', '#formaRegistracija', function(e){
	e.preventDefault();
	
	var username = $(this).find("input[name = username]").val();
	var password = $(this).find("input[name = password]").val();
	var password2 = $(this).find("input[name = password2]").val();
	var name = $(this).find("input[name = name]").val();
	var surname = $(this).find("input[name = surname]").val();
	var email = $(this).find("input[name = email]").val();
	var birthDate = $(this).find("input[name = birthDate]").val();
	var gender = $('#Gender').find(":selected").text();
	var isPrivate = $(this).find("input[name=isPrivate]").is(":checked")?"true":"false";
	
	
	
	if (username == "" || password == "" || name == "" || surname == "" || email == "" || birthDate == "" || gender =="" || password2 ==""){
		alert("Morate popuniti sva polja!");
		return false;
	}	
	if(password != password2){
		alert("Lozinke moraju biti iste!");
		return false;
	}
	
	console.log("registracija");
	$.ajax({
		type : 'POST',
		url : rootURL_reg,
		contentType: 'application/json',
		dataType : 'json',
		data: formToJSON_reg(username, password,  name,  surname,  email,  birthDate, gender, isPrivate),
		success : registerResult,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});	
})
function formToJSON_reg(username, password,  name,  surname, email, birthDate, gender,isPrivate ) {
	return JSON.stringify({
		"username" : username,
		"password" : password,
		"name" : name,
		"surname" : surname,
		"email" : email,
		"birthDate" : birthDate,
		"gender" : gender,
		"isPrivate":isPrivate
	});
}

function formToJSON_log(username, password) {
	return JSON.stringify({
		"username" : username,
		"password" : password
	});
}

function rezultatPrijave(data){
	if(data!=null){
		if(data.isDeleted == true){
			alert("Blokirani ste!");
		}
		else if(data.isAdmin == true){
			
			window.location="../WebProjekat/static/admin.html"
		}
		else{
		window.location = "../WebProjekat/static/korisnik.html";
		}
	}
	else{
		alert("Ne postoji registrovani korisnik sa tim podacima!")
		
	}
}

function registerResult(data){
	if(data!=null){
		window.location = "../static/korisnik.html";
	}
	else{
		alert("Korisnik sa unetim korisnickim imenom vec postoji!");
	}
}