


function drawFriends(data){
	$(".main").empty();
	var friends = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(friends, function(index, friend){
		drawUser(friend);
	
	});
	
}


function drawUser(user){
	var element =  '<div class="userShowcase mock-outer">';
	element+= '<div class="mock-inner">';
	element += '<div class="fb-group-picrow">';
	element += '<img src="'+user.profileImageUrl+'">';
	element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
	element += '<a href="'+user.username+'" id="link_btn"><h5 class="fbh5">'+user.username+'</h5></a></div></div></div>';
	element += '<div class="usertext"><p>'+user.name +' '+user.surname+'</p></div></div>';
	$(".main").append(element);
}


	
function requests(){

	$.ajax({
		type : 'GET',
		url : '../rest/request/getFriendRequests',
		success : drawRequests,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	})};	


function drawRequests(data){
	$(".main").empty();
	var senders = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(senders, function(index, sender){
		drawRequest(sender);
	
	});
}

function drawRequest(user){
	var element =  '<div class="userShowcase mock-outer">';
	element+= '<div class="mock-inner">';
	element += '<div class="fb-group-picrow">';
	element += '<img src="'+user.profileImageUrl+'">';
	element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
	element += '<a href="'+user.username+'" id="link_btn"><h5 class="fbh5">'+user.username+'</h5></a></div></div></div>';
	element += '<div class="inline usertext"><p>'+user.name +' '+user.surname+'</p>';
	element += '<button class="button-pretty sameRowButton" type="button" value="'+user.username+'"id="accept_btn">Prihvati</button>';
	element += '<button class="button-pretty sameRowButton negativeButton" type="button" value="'+user.username+'"id="decline_btn">Odbij</button> </div></div></div>';
	$(".main").append(element);
}


	
	
$(document).on('click', '#accept_btn', function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/request/acceptRequest",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				
				alert("Postali ste prijatelj sa  " + username );	
				location.reload();
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});
	
$(document).on('click', '#decline_btn', function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/request/declineRequest",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: function(){
				location.reload();
				alert("Odbili ste zahtev korisnika : " + username );	
		} ,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});
	
$(document).on('click', '#mutualFriends_btn', function(e){
	e.preventDefault();
 	var username = $(this).val();
	$.ajax({
		type : 'POST',
		url : "../rest/user/mutualFriends",
		contentType: 'application/json',
		dataType : 'json',
		data: JSON.stringify({"username":username}),
		success: mutualFriends,
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	});
	});

function mutualFriends(data){
	$(".main").empty();
	var element = '<div> <h1> Zajednicki Prijatelji</h1></div>';
	var friends = data == null ? [] : (data instanceof Array? data:[data]);
	if (data.length == 0){
		element+= '<div> <h2> Nemate zajednickih prijatelja sa ovim korisnikom</h1></div>';
		}
		
	$(".main").append(element);
	
	$.each(friends, function(index, friend){
		drawUser(friend);
	
	});
	
}