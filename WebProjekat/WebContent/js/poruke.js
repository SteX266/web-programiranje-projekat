function drawUserChats(){
	$.ajax({
			type : 'GET',
			url : '../rest/chat/getCurrentUserChats',
			dataType : 'json',
			success : drawChats,
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				alert("AJAX ERROR: " + errorThrown);
			}
		});
}

function drawChats(data){
	$(".main").empty();
	var chats = data == null ? [] : (data instanceof Array? data:[data]);
	$.each(chats, function(index, chat){
		drawChat(chat);
	
	});
}

function drawChat(user){
	var element =  '<div class="userShowcase mock-outer">';
	element+= '<div class="mock-inner">';
	element += '<div class="fb-group-picrow">';
	element += '<img src="'+user.profileImageUrl+'">';
	element += '<div class="fb-group-text-top">  <div class="fb-group-text">';
	element += '<a href="'+user.username+'" id="link_btn"><h5 class="fbh5">'+user.username+'</h5></a></div></div></div>';
	element += '<div class="inline usertext"><p>'+user.name +' '+user.surname+'</p>';
	element += '<button class="button-pretty sameRowButton moveRight" type="button" value="'+user.username+'"id="chat_btn">Poruke</button>';
	$(".main").append(element);
}





function drawChatUser(username){
		drawTypeBox(username);
		$.ajax({
		url : '../rest/chat/getChat',
        type : "POST",
        contentType : "application/json",
        data : JSON.stringify({"username":username}),
        processData : false,
        success: drawMessages,
        error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	}); 
}

function drawTypeBox(username){
				var element='\
	<section id="app">\
    <div class="container">\
      <div class="row">\
        <div class="col-6">\
          <div class="comment">\
        <p v-for="items in item" v-text="items"></p>\
          </div><!--End Comment-->\
          </div><!--End col -->\
          </div><!-- End row -->\
      <div class="row">\
        <div class="col-6">\
      <textarea type="text" class="input" placeholder="';

	element += "Napisite poruku";
      element+='" v-model="newItem" @keyup.enter="addItem() id="comment_form"></textarea>\
          <button class="primaryContained float-right" type="submit" id="';
	element+='submitMessage_btn"';
      element +='value="'+username+'"">Posalji</button>\
        </div><!-- End col -->\
      </div><!--End Row -->\
    </div><!--End Container -->\
  </section><!-- end App -->\
  ';
  $(".main").append(element);
}

function drawMessages(data){
	var messages = data.messages == null ? [] : (data.messages instanceof Array? data.messages:[data.messages]);
	messages.sort((a,b) => (a.date > b.date)? 1 :-1);
	var myImage = currentUser.profileImageUrl;
	var otherImage="";
	if(data.user1 == currentUser.username){
		$.ajax({
		url : '../rest/user/getUserByUsername',
        type : "POST",
        contentType : "application/json",
        data : JSON.stringify({"username":data.user2}),
        processData : false,
        success: function(user){
        	finalChat(messages, myImage, user.profileImageUrl);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	}); 
	}
	else{
		$.ajax({
		url : '../rest/user/getUserByUsername',
        type : "POST",
        contentType : "application/json",
        data : JSON.stringify({"username":data.user1}),
        processData : false,
        success: function(user){
			
        	finalChat(messages, myImage, user.profileImageUrl);
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("AJAX ERROR: " + errorThrown);
		}
	}); 
	}

}

function finalChat(messages, myImage, otherImage){
	
		$.each(messages, function(index, message){
		if(message.sender == currentUser.username){
			drawMyMessage(message, myImage);
		}
		else{
			drawOtherMessage(message, otherImage);
		}
		
	});
	
}
function drawMyMessage(message, image){
		var date = new Date(message.date);
        var element = '<div class="chatContainer darker">';
		element += '<img src="'+image+'" alt="Avatar" class="right" style="width:100%;"><a href="'+message.sender+'"id="link_btn">'+message.sender+'</a></img>';
		element += '<p>'+message.messageText+'</p>';
		element += '<span class="time-left">'+date.toISOString().substring(0,10)+'</span></div>';
		$(".bigContainer").append(element);

  
}
function drawOtherMessage(message, image){
	
		var date = new Date(message.date);
			var element ='<div class="chatContainer">';
			element += '<img src="'+image+'" alt="Avatar" style="width:100%;"><a class="left" href="'+message.sender+'" "id="link_btn">'+message.sender+'</a></img>';
			element += '<p>'+message.messageText+'</p>';
			element += '<span class="time-right">'+date.toISOString().substring(0,10)+'</span></div>';
			$(".bigContainer").append(element);

}

$(document).on('click', '#submitMessage_btn', function(e){
	var username = $(this).val();
	var text = $('textarea').val();
		$.ajax({
		type : 'POST',
		url : '../rest/chat/sendMessage',
		contentType:'application/json',
		dataType: 'json',
		data:JSON.stringify({"sender":currentUser.username,"reciver":username,"messageText":text}),
		success : function(){
			location.reload();
		},
		error : function(XMLHttpRequest, textStatus, errorThrown) {
			alert("bas odavde dobijam AJAX ERROR: " + errorThrown);
		}
	});	
	
	});

