package services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.UserDAO;
import model.Comment;
import model.Post;
import model.User;

@Path("post")
public class PostService {
	@Context
	ServletContext ctx;
	@Context
	HttpServletRequest request;
	UserDAO userDAO; 
	
	@GET
	@Path("/getFriendsPosts")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Post> getFriendsPosts(){
		ArrayList<Post>posts = new ArrayList<Post>();
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		User user = getCurrentUser();
		
		for(String friendUsername:user.getFriends()) {
			User friend = userDAO.find(friendUsername);
				for(Post post:friend.getPosts()) {
					System.out.println(post.toString());
					if(!post.getIsDeleted()) {
						for (Comment c : post.getComments()) {
							if (c.getIsDeleted()) {
								post.removeComment(c);
							}
						}
						posts.add(post);
					}
					
			}	
		}
		return posts;
	}
	
	@POST
	@Path("/getUserPosts")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<Post> getCurrentUserPosts(User user){
		
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		ArrayList<Post> posts = new ArrayList<>();
		for (Post post : userDAO.find(user.getUsername()).getPosts()) {
			if(!post.getIsDeleted()) {
				for (Comment c : post.getComments()) {
					if (c.getIsDeleted()) {
						post.removeComment(c);
					}
				}
				posts.add(post);
			}
		}
		return posts;
	}
	
	@POST
	@Path("/getPostById")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Post getPostById(Post postId) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		for (User user : users) {
			for (Post post : user.getPosts()) {
				if(post.getId()==(postId.getId())) {
					return post;
			}
			}
		}
		return null;
	}
	
	@POST
	@Path("/postPost")
	@Consumes(MediaType.APPLICATION_JSON)
	public void changeUserInfo(Post post) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		int id = generatePostId();
		String realUrl = null;
		if(post.getPicture() != null) {
			realUrl = "images\\".concat(post.getPicture());
			realUrl = "..\\".concat(realUrl);
		}

		User u = (User) session.getAttribute("user");
		
		Post p = new Post(id,realUrl, post.getText(), u.getUsername(), u.getProfileImageUrl());
		for (User user:userDAO.findAll()) {
			if(u.getUsername().equals(user.getUsername())) {
				user.addPost(p);
			}
		}
		userDAO.saveUsers();		
	}
	
	private int generatePostId() {
		int number = 0;
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		for(User u:userDAO.findAll()) {
			for(Post p:u.getPosts()) {
				number++;
			}
		}
		return number+1;
	}
	
	@POST
	@Path("/deletePost")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deletePost(Post p) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		for (User user:users) {
			for(Post post:user.getPosts()) {
				if(post.getId() == p.getId()) {
					post.setIsDeleted(true);
					userDAO.saveUsers();
					return;
				}
			}
		}
	}
	
	@GET
	@Path("/getCurrentUser")
	@Produces(MediaType.APPLICATION_JSON)
	public User getCurrentUser() {
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		
		User user = userDAO.find(u.getUsername());
		return user;
	}
	
}
