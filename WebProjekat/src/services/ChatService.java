package services;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import dao.ChatDAO;
import dao.UserDAO;
import model.Chat;
import model.FriendRequest;
import model.Message;
import model.User;
@Path("chat")
public class ChatService {
		@Context
		ServletContext ctx;
		@Context
		HttpServletRequest request;
		ChatDAO chatDAO; 
		UserDAO userDAO;
		
		
		@GET
		@Path("/getCurrentUser")
		@Produces(MediaType.APPLICATION_JSON)
		public User getCurrentUser() {
			HttpSession session = request.getSession();
			User u = (User) session.getAttribute("user");
			userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
			List<User> users = (ArrayList<User>) userDAO.findAll();
			
			User user = userDAO.find(u.getUsername());
			return user;
		}
		
		@GET
		@Path("/getCurrentUserChats")
		@Produces(MediaType.APPLICATION_JSON)
		public ArrayList<User> getCurrentUserChats() {
			ArrayList<User> userChats = new ArrayList<>();
			User current = getCurrentUser();
			HttpSession session = request.getSession();
			userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
			chatDAO = new ChatDAO(session.getServletContext().getRealPath("") + File.separator+"chats.json");
			for (Chat chat : chatDAO.findAll()) {
				if(chat.getUser1().equals(current.getUsername())){
					userChats.add(userDAO.find(chat.getUser2()));	
				}
				else if(chat.getUser2().equals(current.getUsername())) {
					userChats.add(userDAO.find(chat.getUser1()));	
				}
			}
			
		return userChats;
		}
		
		@POST
		@Path("/getChat")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Chat getChats(User u) {
			User current = getCurrentUser();
			HttpSession session = request.getSession();
			chatDAO = new ChatDAO(session.getServletContext().getRealPath("") + File.separator+"chats.json");
			Chat c = chatDAO.find(current.getUsername(), u.getUsername());
			if(c== null) {
				c = new Chat(generateChatId(), current.getUsername(), u.getUsername(), new ArrayList<Message>());
			}
			return c;
		}
		
		private int generateChatId() {
			HttpSession session = request.getSession();
			chatDAO = new ChatDAO(session.getServletContext().getRealPath("") + File.separator+"chats.json");
			return chatDAO.findAll().size();

		}
		
		@POST
		@Path("/sendMessage")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public void sendMessage(Message m) {
			HttpSession session = request.getSession();
			chatDAO = new ChatDAO(session.getServletContext().getRealPath("") + File.separator+"chats.json");
			Chat c = chatDAO.find(m.getSender(),m.getReciver());
			if(c== null) {
				c = new Chat(generateChatId(),m.getSender(),m.getReciver(),new ArrayList<Message>());
				chatDAO.addChat(c);
			}
			m.setDate(new Date(System.currentTimeMillis()));
			c.addMessage(m);
			chatDAO.saveChats();
			return;
		}

}
