package services;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import classes.CommentToPost;
import dao.UserDAO;
import model.Comment;
import model.Post;
import model.User;

@Path("comment")
public class CommentService {
	@Context
	ServletContext ctx;
	@Context
	HttpServletRequest request;
	UserDAO userDAO; 
	
	@POST
	@Path("/postComment")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Comment postComment(CommentToPost commentToPost) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User u = (User) session.getAttribute("user");
		int id = createCommentId();
		Date date = new Date(System.currentTimeMillis());
		
		Comment comment = new Comment(id,u.getUsername(),date, commentToPost.getText());
		for (User user:userDAO.findAll()) {

			for(Post post:user.getPosts()) {
				if(post.getId() == commentToPost.getPostId()) {
					post.addComment(comment);
					userDAO.saveUsers();
					break;
				}
			}
		}
		return comment;
	}
	private int createCommentId() {
		int number = 0;
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		for (User user : userDAO.findAll()) {
			for(Post post:user.getPosts()) {
				for(Comment comment:post.getComments()) {
					number++;
				}
			}
		}
		return number+1;
	}
	@POST
	@Path("/deleteComment")
	@Consumes(MediaType.APPLICATION_JSON)
	public void deleteComment(Comment c) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		for (User user:users) {
			for(Post post:user.getPosts()) {
				for(Comment comment:post.getComments()) {
					if(c.getId() == comment.getId()) {
						comment.setDeleted(true);
						userDAO.saveUsers();
						return;
					}
				}
			}
		}
	}
}
