package services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import classes.UserToPresent;
import dao.UserDAO;
import model.FriendRequest;
import model.FriendRequestStatus;
import model.User;

@Path("request")
public class RequestService {
	@Context
	ServletContext ctx;
	@Context
	HttpServletRequest request;
	UserDAO userDAO; 
	
	

	@GET
	@Path("/getFriendRequests")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getFriendRequests() {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		ArrayList<User> senders = new ArrayList<>();
		User u = getCurrentUser();
		List<FriendRequest> requests = u.getFriendRequests();
		for (FriendRequest request : requests) {
			if (!request.getIsDeleted() && request.getStatus() == FriendRequestStatus.ON_HOLD) {
				User sender = userDAO.find(request.getSender());
				senders.add(sender);
			}
		}
		return senders;
	}
	
	



	private int generateRequestId() {
		int number = 0;
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		for(User u:userDAO.findAll()) {
			for(FriendRequest p:u.getFriendRequests()) {
				number++;
			}
		}
		return number+1;
	}
	
	@POST
	@Path("/acceptRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public void acceptRequest(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User current = getCurrentUser();
		for (FriendRequest request : current.getFriendRequests()) {
			if (request.getSender().equals(user.getUsername()) && request.getStatus() == FriendRequestStatus.ON_HOLD && !request.getIsDeleted()) {
				request.setStatus(FriendRequestStatus.ACCEPTED);
				current.addFriend(user.getUsername());
				User u2 = userDAO.find(user.getUsername());
				u2.addFriend(current.getUsername());
				userDAO.saveUsers();
			}
		
		}
		
	
	}
	
	@POST
	@Path("/declineRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public void declineRequest(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User current = getCurrentUser();
		for (FriendRequest request : current.getFriendRequests()) {
			if (request.getSender().equals(user.getUsername()) && request.getStatus() == FriendRequestStatus.ON_HOLD && !request.getIsDeleted()) {
				request.setStatus(FriendRequestStatus.DECLINED);
				userDAO.saveUsers();
				
			}
		}
		
	
	}
	
	public User getCurrentUser() {
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		if(u == null) {
			return null;
		}
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		
		User user = userDAO.find(u.getUsername());
		return user;
	}
	
	@POST
	@Path("/sendRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public void sendRequest(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User u = userDAO.find(user.getUsername());
		User current = getCurrentUser();
		FriendRequest request = new FriendRequest(generateRequestId(), current.getUsername(), u.getUsername());
		userDAO.addRequest(request, u.getUsername());
	}
	
	@POST
	@Path("/getUserToPresent")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public UserToPresent getUserToPresent(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User u = userDAO.find(user.getUsername());
		User current = getCurrentUser();
		if (current ==null) {
			return new UserToPresent(u, false, false, false ,false);
		}
		int exists = requestExists(current, u);
		boolean isLog = true; 
		boolean isFriend = u.isFriend(current);
		boolean sent = false;
		boolean recived = false;
		if (exists == 1) {
			recived = true;
		}
		if (exists == 2) {
			sent = true;
		}
		UserToPresent up = new UserToPresent(u, isFriend, isLog,sent,recived);
		return up;
		
	}
	
	private int requestExists(User sender, User reciver) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		for(FriendRequest r1 : sender.getFriendRequests()) {
			if (r1.getSender().equals(reciver.getUsername()) && r1.getStatus() == FriendRequestStatus.ON_HOLD && !r1.getIsDeleted()) {
				return 1;
			}
		}
		for(FriendRequest r2 : reciver.getFriendRequests()) {
			if (r2.getSender().equals(sender.getUsername()) && r2.getStatus() == FriendRequestStatus.ON_HOLD && !r2.getIsDeleted()) {
				return 2;			
			}	
		}
		return 69 ;
	}
	
	
	@POST
	@Path("/cancelRequest")
	@Consumes(MediaType.APPLICATION_JSON)
	public void cancelRequest(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User current = getCurrentUser();
		User u = userDAO.find(user.getUsername());
		for(FriendRequest r : u.getFriendRequests()){
			if (r.getSender().equals(current.getUsername())) {
				r.setStatus(FriendRequestStatus.DECLINED);
			}
			
		}
		userDAO.saveUsers();	
	}
	

}
