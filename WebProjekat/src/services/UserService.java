package services;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import classes.SearchParameters;
import classes.UserToPresent;
import dao.UserDAO;
import model.FriendRequest;
import model.FriendRequestStatus;
import model.Post;
import model.User;

@Path("user")
public class UserService {
	@Context
	ServletContext ctx;
	@Context
	HttpServletRequest request;
	UserDAO userDAO; 
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User login(User userCredentials) {
		HttpSession session = request.getSession();
		
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		
		List<User> users = (ArrayList<User>) userDAO.findAll();
		
		for (User user : users) {
			if (user.getUsername().equals(userCredentials.getUsername()) && user.getPassword().equals(userCredentials.getPassword())) {
				session.setAttribute("user", user);
				return user;
			}
		}
		
		return null;
	}
	
	
	@POST
	@Path("/register")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User register(User userToRegister) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		for (User user : users) {
			System.out.println(user.getUsername());

			if(user.getUsername().equals(userToRegister.getUsername())) {
				return null;
			}
		}
		User u = new User(userToRegister.getUsername(), userToRegister.getPassword(), userToRegister.getEmail(), userToRegister.getName(),userToRegister.getSurname(),userToRegister.getBirthDate(),userToRegister.getGender(),userToRegister.getIsPrivate());
		userDAO.addUser(u);
		session.setAttribute("user", u);
		return u;
		
	}
	
	

	
	@GET
	@Path("/logout")
	public void logout() {
		System.out.println("ODJAVA");
		HttpSession session = request.getSession();
		session.invalidate();
	}
		
	@GET
	@Path("/getFriendList")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getFriendList(){
		ArrayList<User>users = new ArrayList<User>();
		HttpSession session = request.getSession();
		User user= getCurrentUser();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		for (String username : user.getFriends()) {
			User u = userDAO.find(username);
			if (u!=null) {
				if (u.getIsDeleted()) {
					continue;	
				}
				else {
					users.add(u);
				}
			}
		}	
		return users;

	}
	
	

	
	@GET
	@Path("/getCurrentUser")
	@Produces(MediaType.APPLICATION_JSON)
	public User getCurrentUser() {
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		if(u == null) {
			return null;
		}
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		
		User user = userDAO.find(u.getUsername());
		return user;
	}
	
	@GET
	@Path("/getAllUsers")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> getAllUsers() {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		ArrayList<User> users = (ArrayList<User>) userDAO.findAll();
		return users;
	}
	
	
	@POST
	@Path("/changeUserInfo")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public void changeUserInfo(User userToChange) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		for (User user : users) {
			if(user.getUsername().equals(userToChange.getUsername())) {
				user.setPassword(userToChange.getPassword());
				user.setEmail(userToChange.getEmail());
				user.setName(userToChange.getName());
				user.setSurname(userToChange.getSurname());
				user.setBirthDate(userToChange.getBirthDate());
				user.setGender(userToChange.getGender());
				user.setIsPrivate(userToChange.getIsPrivate());
				break;
			}
		}

		userDAO.saveUsers();
		
	}
	
	
	@POST
	@Path("/changeImage")
	@Consumes(MediaType.APPLICATION_JSON)
	public void changeUserInfo(String imageUrl) {
		String realUrl = generateImageUrl(imageUrl);
		User u = getCurrentUser();
		System.out.println(realUrl);
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> users = (ArrayList<User>) userDAO.findAll();
		
		for (User user : users) {
			if(user.getUsername().equals(u.getUsername())) {
				user.setProfileImageUrl(realUrl);
				for (Post post:user.getPosts()) {
					post.setUserPicture(realUrl);
				}
				userDAO.saveUsers();
				break;
			}
		}
	}
	


	
	@POST
	@Path("/getUserByUsername")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User getUserByUsername(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User u = userDAO.find(user.getUsername());
		return u;
	}
	




	private String generateImageUrl(String image) {
		String[] tokens = image.split(":");
		String url = tokens[1];
		
		url = url.substring(1, url.length() - 2);
		String realUrl = "images\\".concat(url);
		realUrl = "..\\".concat(realUrl);
		
		return realUrl;
		
	}

	@POST
	@Path("/removeFriend")
	@Consumes(MediaType.APPLICATION_JSON)
	public void removeFriend(User user) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		System.out.println("uso");
		User current = getCurrentUser();
		User u = userDAO.find(user.getUsername());
		current.removeFriend(user.getUsername());
		u.removeFriend(current.getUsername());		
		userDAO.saveUsers();		
		
	}

	@POST
	@Path("/search")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<User> search(SearchParameters searchParameters) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		List<User> allUsers = (ArrayList<User>) userDAO.findAll();
		ArrayList<User> users = new ArrayList<User>(); 

		for (User u:allUsers) {
			if(u.getName().contains(searchParameters.getName())&& u.getSurname().contains(searchParameters.getSurname())) {
				if(searchParameters.getDateFrom() == null || searchParameters.getDateTo()==null) {
					users.add(u);
				}
				else if(searchParameters.getDateFrom().before(u.getBirthDate()) && searchParameters.getDateTo().after(u.getBirthDate())) {
					users.add(u);
				}
			}
		}
		

		
		
		return users;
		
	}
	
	@POST
	@Path("/mutualFriends")
	@Consumes(MediaType.APPLICATION_JSON)
	public ArrayList<User> mutualFriends(User user) {
		ArrayList<User> mutualFriends = new ArrayList<>(); 
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User current = getCurrentUser();
		User u = userDAO.find(user.getUsername());
		for(String  f1 : u.getFriends()){
			for(String  f2 : current.getFriends()){
				if(f1.equals(f2)) {
					mutualFriends.add(userDAO.find(f1));
				}
			}
		}
		return mutualFriends;
	}


	
	@POST
	@Path("/blockUser")
	@Consumes(MediaType.APPLICATION_JSON)
	public void blockUser(User u) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User user = userDAO.find(u.getUsername());
		user.setDeleted(true);
		userDAO.saveUsers();
		}

	@POST
	@Path("/unblockUser")
	@Consumes(MediaType.APPLICATION_JSON)
	public void unblockUser(User u) {
		HttpSession session = request.getSession();
		userDAO = new UserDAO(session.getServletContext().getRealPath("") + File.separator+"users.json");
		User user = userDAO.find(u.getUsername());
		user.setDeleted(false);
		userDAO.saveUsers();
		}
}


