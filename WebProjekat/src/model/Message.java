package model;

import java.util.Date;

public class Message {
	
	private int id;
	private String sender;
	private String reciver;
	private String messageText;
	private Date date;
	private boolean isDeleted;
	
	public Message() {
		super();
	}

	

	public Message(int id, String sender, String reciver, String messageText, Date date) {
		super();
		this.id = id;
		this.sender = sender;
		this.reciver = reciver;
		this.messageText = messageText;
		this.date = date;
		this.isDeleted = false;
	}



	public Message(int id, String sender, String reciver, String messageText, Date date, boolean isDeleted) {
		super();
		this.id = id;
		this.sender = sender;
		this.reciver = reciver;
		this.messageText = messageText;
		this.date = date;
		this.isDeleted = isDeleted;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getSender() {
		return sender;
	}



	public void setSender(String sender) {
		this.sender = sender;
	}



	public String getReciver() {
		return reciver;
	}



	public void setReciver(String reciver) {
		this.reciver = reciver;
	}



	public String getMessageText() {
		return messageText;
	}



	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}



	public Date getDate() {
		return date;
	}



	public void setDate(Date date) {
		this.date = date;
	}



	public boolean getIsDeleted() {
		return isDeleted;
	}



	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

}