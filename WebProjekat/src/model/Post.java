package model;

import java.util.ArrayList;
import java.util.List;

public class Post {
	
	private int id;
	private String picture;
	private String text;
	private List<Comment> comments;
	private String username;
	private String userPicture;
	private Boolean isDeleted;
	
	public Post() {
		super();
	}

	

	public Post(int id, String text, String username, String userPicture) {
		super();
		this.id = id;
		this.text = text;
		this.comments = new ArrayList<Comment>();
		this.picture = null;
		this.username =username;
		this.userPicture = userPicture;
		this.isDeleted = false;
	}


	
	public Post(int id, String picture, String text, String username, String userPicture) {
		super();
		this.id = id;
		this.picture = picture;
		this.text = text;
		this.comments = new ArrayList<Comment>();
		this.username = username;
		this.userPicture = userPicture;
		this.isDeleted = false;
	}


	

	

	public Post(int id, String picture, String text, List<Comment> comments, String username, String userPicture) {
		super();
		this.id = id;
		this.picture = picture;
		this.text = text;
		this.comments = comments;
		this.username = username;
		this.userPicture = userPicture;
		this.isDeleted = false;
	}



	public Post(int id, String picture, String text, List<Comment> comments, String username, String userPicture,
			Boolean isDeleted) {
		super();
		this.id = id;
		this.picture = picture;
		this.text = text;
		this.comments = comments;
		this.username = username;
		this.userPicture = userPicture;
		this.isDeleted = isDeleted;
	}



	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public String getUsername() {
		return username;
	}



	public void setUsername(String username) {
		this.username = username;
	}



	public String getUserPicture() {
		return userPicture;
	}



	public void setUserPicture(String userPicture) {
		this.userPicture = userPicture;
	}



	public Boolean getIsDeleted() {
		return isDeleted;
	}



	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public void addComment(Comment comment) {
		this.comments.add(comment);
	}
	
	public void removeComment(Comment comment) {
		this.comments.remove(comment);
	}



	@Override
	public String toString() {
		return "Post [id=" + id + ", picture=" + picture + ", text=" + text + ", comments=" + comments + ", username="
				+ username + ", userPicture=" + userPicture + ", isDeleted=" + isDeleted + "]";
	}
	
}
