package model;

import java.util.Date;

public class Comment {
	
	private int id;
	private String user;
	private Date originalDate;
	private Date modificationDate;
	private boolean isDeleted;
	private String text;
	
	public Comment() {
		super();
	}

	

	public Comment(int id, String user, Date originalDate, String text) {
		super();
		this.id = id;
		this.user = user;
		this.originalDate = originalDate;
		this.modificationDate = null;
		this.isDeleted = false;
		this.text = text;
		
	}



	public Comment(int id, String user, Date originalDate, Date modificationDate, String text) {
		super();
		this.id = id;
		this.user = user;
		this.originalDate = originalDate;
		this.modificationDate = modificationDate;
		this.isDeleted = false;
		this.text=text;
	}


	

	public Comment(int id, String user, Date originalDate, Date modificationDate, boolean isDeleted, String text) {
		super();
		this.id = id;
		this.user = user;
		this.originalDate = originalDate;
		this.modificationDate = modificationDate;
		this.isDeleted = isDeleted;
		this.text = text;
	}



	public String getText() {
		return text;
	}



	public void setText(String text) {
		this.text = text;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getUser() {
		return user;
	}



	public void setUser(String user) {
		this.user = user;
	}



	public Date getOriginalDate() {
		return originalDate;
	}



	public void setOriginalDate(Date originalDate) {
		this.originalDate = originalDate;
	}



	public Date getModificationDate() {
		return modificationDate;
	}



	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}



	public boolean getIsDeleted() {
		return isDeleted;
	}



	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
}