package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
	private String username;
	private String password;
	private String email;
	private String name;
	private String surname;
	private Date birthDate;
	private String gender;
	private Boolean isAdmin;
	private String profileImageUrl;
	private List<Post> posts;
	private List<FriendRequest> friendRequests;
	private List<String> friends;
	private Boolean isPrivate;
	private boolean isDeleted;
	
	public User() {
		this.friends = new ArrayList<String>();
	}
	
	
	public User(String username, String password, String email, String name, String surname, Date birthDate,
			String gender, Boolean isAdmin, String profileImageUrl, List<Post> posts,
			List<FriendRequest> friendRequests, List<String> friends, Boolean isPrivate) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.gender = gender;
		this.isAdmin = isAdmin;
		this.profileImageUrl = profileImageUrl;
		this.posts = posts;
		this.friendRequests = friendRequests;
		this.friends = friends;
		this.isPrivate = isPrivate;
		this.isDeleted = false;
				}


	public User(String username, String password, String email, String name, String surname, Date birthDate,
			String gender, Boolean isPrivate) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.gender = gender;
		this.isPrivate = isPrivate;
		this.isAdmin = false;
		this.profileImageUrl = "";
		this.posts = new ArrayList<Post>();
		this.friendRequests = new ArrayList<FriendRequest>();
		this.friends = new ArrayList<String>();
		this.isDeleted = false;
	}


	
	public User(String username, String password, String email, String name, String surname, Date birthDate,
			String gender, Boolean isAdmin, String profileImageUrl, List<Post> posts,
			List<FriendRequest> friendRequests, List<String> friends, Boolean isPrivate, boolean isDeleted) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.gender = gender;
		this.isAdmin = isAdmin;
		this.profileImageUrl = profileImageUrl;
		this.posts = posts;
		this.friendRequests = friendRequests;
		this.friends = friends;
		this.isPrivate = isPrivate;
		this.isDeleted = isDeleted;
	}


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Boolean getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}


	public List<String> getFriends() {
		return friends;
	}

	public void setFriends(List<String> friends) {
		this.friends = friends;
	}

	public Boolean getIsPrivate() {
		return isPrivate;
	}

	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<FriendRequest> getFriendRequests() {
		return friendRequests;
	}

	public void setFriendRequests(List<FriendRequest> friendRequests) {
		this.friendRequests = friendRequests;
	}


	public boolean getIsDeleted() {
		return isDeleted;
	}


	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public void addPost(Post post) {
		this.posts.add(post);
		
	}
	public void addRequest(FriendRequest r) {
		this.friendRequests.add(r);
		
	}
	public void addFriend(String f) {
		this.friends.add(f);
		
	}
	
	public void removeFriend(String f) {
		for (int i = 0 ; i < friends.size(); i ++ ) {
			if (friends.get(i).equals(f)) {
				friends.remove(i);
			}
		}
	}
	
	public boolean isFriend(User u) {
		for (String friend : friends) {
			if( u.getUsername().equals(friend)){
				return true;
			}
		}
		return false;
	}
	
	
}
