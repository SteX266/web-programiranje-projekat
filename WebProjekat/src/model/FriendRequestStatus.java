package model;

import com.fasterxml.jackson.annotation.JsonFormat;


public enum FriendRequestStatus {
ON_HOLD,
ACCEPTED,
DECLINED
}
