package model;

public class FriendRequest {

	private int id;
	private String sender;
	private String reciver;
	private  FriendRequestStatus status;
	private boolean isDeleted;
	
	public FriendRequest() {
		super();
	}

	public FriendRequest(int id, String sender, String reciver) {
		super();
		this.id = id;
		this.sender = sender;
		this.reciver = reciver;
		this.status = FriendRequestStatus.ON_HOLD;
		this.isDeleted = false;
	}

	public FriendRequest(int id, String sender, String reciver, FriendRequestStatus status) {
		super();
		this.id = id;
		this.sender = sender;
		this.reciver = reciver;
		this.status = status;
		this.isDeleted = false;
	}

	
	public FriendRequest(int id, String sender, String reciver, FriendRequestStatus status, boolean isDeleted) {
		super();
		this.id = id;
		this.sender = sender;
		this.reciver = reciver;
		this.status = status;
		this.isDeleted = isDeleted;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReciver() {
		return reciver;
	}

	public void setReciver(String reciver) {
		this.reciver = reciver;
	}

	public FriendRequestStatus getStatus() {
		return status;
	}

	public void setStatus(FriendRequestStatus status) {
		this.status = status;
	}

	public boolean getIsDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	
}
