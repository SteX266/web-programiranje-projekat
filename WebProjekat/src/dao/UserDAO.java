package dao;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import model.FriendRequest;
import model.Post;
import model.User;

public class UserDAO {

		private List<User> users = new ArrayList<User>();
		private String path;
		private  ObjectMapper mapper = new ObjectMapper();
		
		public UserDAO() {
			this.path = "users.json";
			loadUsers();
		}


		public UserDAO(String contextPath) {
			this.path = contextPath;
			loadUsers();
		}


		public User find(String username) {
			
			for (User user : users) {
				if(user.getUsername().equals(username)) {
					return user;
				}
			}
			return null;

		}

		public List<User> findAll() {
			return users;
		}
		
		public Boolean usernameExists(String username) {
			for (User user : users) {
				if(user.getUsername().equals(username)) {
					return true;
				}
			}
			return false;
		}
		public void addUser(User user) {
			if(!usernameExists(user.getUsername())) {
				users.add(user);
				saveUsers();
			}

		}

		
		public void saveUsers() {
			ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
		    try {
				writer.writeValue(Paths.get(path).toFile(), users);
			} catch (JsonGenerationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		public void loadUsers() {
			 ObjectMapper mapper = new ObjectMapper();
			 User[] us;
			try {
				us = mapper.readValue(Paths.get(path).toFile(), User[].class);
			} catch (Exception e) {
				System.out.println("EXCEPTION!");
				e.printStackTrace();
				System.out.println(e);
				return;
			}
			
			 for (User user : us) {
				 users.add(user);
				}
			}
		     
		public void addRequest(FriendRequest r, String username) {
			for (User user : users) {
				if(user.getUsername().equals(username)) {
					user.addRequest(r);
					break;
				}
				
			}
			saveUsers();
		}
		
		public ArrayList<Post> FriendsPosts(User u){
			if (!users.contains(u))
				return null;
			ArrayList<Post> posts = new ArrayList<>();
			for (String username : u.getFriends()) {
				User friend = this.find(username);
				for (Post post : friend.getPosts()) {
					if(!post.getIsDeleted()) {
						posts.add(post);
					}
				}
			}
			return posts;
		} 
}
