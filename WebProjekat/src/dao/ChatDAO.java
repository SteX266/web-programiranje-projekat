package dao;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import model.Chat;

public class ChatDAO {

	private List<Chat> chats = new ArrayList<Chat>();
	private String path;
	private  ObjectMapper mapper = new ObjectMapper();
	
	public ChatDAO() {
		this.path = "chats.json";
		loadChats();
	}


	public ChatDAO(String contextPath) {
		this.path = contextPath;
		loadChats();
	}


	public Chat find(String u1,String u2) {
		if (u1.equals(u2)) 
			return null;
		for (Chat chat : chats) {
			if((chat.getUser1().equals(u1) && chat.getUser2().equals(u2)) || chat.getUser1().equals(u2) && chat.getUser2().equals(u1)) {
				return chat;
			}
		}
		return null;

	}

	public List<Chat> findAll() {
		return chats;
	}
	
	public Boolean chatExists(String u1, String u2) {
		if (find(u1,u2) != null) {
			return true;
		}
		return false;
	}
	
	public void addChat(Chat chat) {
		if(!chatExists(chat.getUser1(),chat.getUser2())) {
			chats.add(chat);
			saveChats();
		}

	}

	
	public void saveChats() {
		ObjectWriter writer = mapper.writer(new DefaultPrettyPrinter());
	    try {
			writer.writeValue(Paths.get(path).toFile(), chats);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private void loadChats() {
		 ObjectMapper mapper = new ObjectMapper();
		 Chat[] us;
		try {
			us = mapper.readValue(Paths.get(path).toFile(), Chat[].class);
		} catch (Exception e) {
			System.out.println("EXCEPTION!");
			e.printStackTrace();
			System.out.println(e);
			return;
		}
		
		 for (Chat chat : us) {
			 chats.add(chat);
			}
		}
	    
}

