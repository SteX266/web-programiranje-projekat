package classes;

public class CommentToPost {
	int postId;
	String text;
	
	public CommentToPost() {
		super();
	}
	public CommentToPost(int postId, String text) {
		super();
		this.postId = postId;
		this.text = text;
	}
	public int getPostId() {
		return postId;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
	
}
