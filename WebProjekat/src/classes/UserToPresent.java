package classes;

import model.User;

public class UserToPresent {
	User user;
	boolean isFriend;
	boolean isLogin;
	boolean isSent;
	boolean isRecived;
	
	public UserToPresent() {
		super();
	}
	
	public UserToPresent(User user, boolean isFriend, boolean isLogin ,boolean isSent,	boolean isRecived) {
		super();
		this.user = user;
		this.isFriend = isFriend;
		this.isLogin = isLogin;
		this.isSent = isSent;
		this.isRecived = isRecived;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isFriend() {
		return isFriend;
	}

	public void setFriend(boolean isFriend) {
		this.isFriend = isFriend;
	}

	public boolean isLogin() {
		return isLogin;
	}

	public void setLogin(boolean isLogin) {
		this.isLogin = isLogin;
	}

	public boolean isSent() {
		return isSent;
	}

	public void setSent(boolean isSent) {
		this.isSent = isSent;
	}

	public boolean isRecived() {
		return isRecived;
	}

	public void setRecived(boolean isRecived) {
		this.isRecived = isRecived;
	}

	
	
	
}
