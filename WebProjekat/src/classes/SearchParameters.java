package classes;

import java.util.Date;

public class SearchParameters {
	String name;
	String surname;
	Date dateFrom;
	Date dateTo;
	
	public SearchParameters() {
		super();
	}
	
	public SearchParameters(String name, String surname, Date dateFrom, Date dateTo) {
		super();
		this.name = name;
		this.surname = surname;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Date getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}
	public Date getDateTo() {
		return dateTo;
	}
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}
	
	
}
